package com.example;

import java.util.Scanner;

public class Array {
    int[] array;
    public void initializeArray(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter the Size of the Array:- ");
        int arraySize=scanner.nextInt();
        array=new int[arraySize];
        System.out.println("Enter the Array Elements:- ");
        for (int iterate = 0; iterate < array.length; iterate++) {
            array[iterate] = scanner.nextInt();
        }
    }
    public int checkArrayType(){
        boolean odd=false;
        boolean even=false;
        for (int iterate = 0; iterate < array.length; iterate++) {
            if(array[iterate]%2==0){
                even=true;
            }
            else{
                odd=true;
            }
        }
        if(even==true && odd == false){
            return 1;
        }
        else if(odd==true && even==false){
            return 2;
        }
        else if(even == true && odd == true){
            return 3;
        }
        return 0;
    }
}
